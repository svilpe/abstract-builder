package lv.dbguys;

/**
 * Created by boris on 30/06/16.
 */
public class Car extends Vehicle {
    private double engineVolume;
    private boolean automaticTransmission;

    public double getEngineVolume() {
        return engineVolume;
    }

    public void setEngineVolume(double engineVolume) {
        this.engineVolume = engineVolume;
    }

    public boolean isAutomaticTransmission() {
        return automaticTransmission;
    }

    public void setAutomaticTransmission(boolean automaticTransmission) {
        this.automaticTransmission = automaticTransmission;
    }
}
