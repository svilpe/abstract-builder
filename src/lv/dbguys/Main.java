package lv.dbguys;

import lv.dbguys.builder.BicycleBuilder;
import lv.dbguys.builder.CarBuilder;

public class Main {

    public static void main(String[] args) {
        Car car = CarBuilder.aCar().withPeopleCapacity(4).withAutomaticTransmission(true).withWeight(1.5).build();
        Bicycle bicycle = BicycleBuilder.aBycicle().withFrameSize(7.55).withMaxSpeed(30).withPeopleCapacity(1).build();


    }
}
