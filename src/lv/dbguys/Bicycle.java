package lv.dbguys;

/**
 * Created by boris on 30/06/16.
 */
public class Bicycle extends Vehicle {
    private double frameSize;
    private boolean withSpeeds;

    public double getFrameSize() {
        return frameSize;
    }

    public void setFrameSize(double frameSize) {
        this.frameSize = frameSize;
    }

    public boolean isWithSpeeds() {
        return withSpeeds;
    }

    public void setWithSpeeds(boolean withSpeeds) {
        this.withSpeeds = withSpeeds;
    }
}
