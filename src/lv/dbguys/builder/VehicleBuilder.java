package lv.dbguys.builder;

import lv.dbguys.Vehicle;

/**
 * Created by boris on 30/06/16.
 */
public abstract class VehicleBuilder<B extends VehicleBuilder, T extends Vehicle> {
    private int peopleCapacity;
    private int maxSpeed;
    private double weight;

    public B withPeopleCapacity(int peopleCapacity) {
        this.peopleCapacity = peopleCapacity;
        return (B) this;
    }

    public B withMaxSpeed(int maxSpeed) {
        this.maxSpeed = maxSpeed;
        return (B) this;
    }

    public B withWeight(double weight) {
        this.weight = weight;
        return (B) this;
    }

    public T build() {
        T vehicle = realization();
        vehicle.setPeopleCapacity(peopleCapacity);
        vehicle.setMaxSpeed(maxSpeed);
        vehicle.setWeight(weight);
        return vehicle;
    }

    protected abstract T realization();


}
