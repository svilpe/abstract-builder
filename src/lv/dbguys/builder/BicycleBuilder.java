package lv.dbguys.builder;

import lv.dbguys.Bicycle;

/**
 * Created by boris on 01/07/16.
 */
public final class BicycleBuilder extends VehicleBuilder<BicycleBuilder, Bicycle> {
    private double frameSize;
    private boolean withSpeeds;

    private BicycleBuilder() {
    }

    public static BicycleBuilder aBycicle() {
        return new BicycleBuilder();
    }

    public BicycleBuilder withFrameSize(double frameSize) {
        this.frameSize = frameSize;
        return this;
    }

    public BicycleBuilder withWithSpeeds(boolean withSpeeds) {
        this.withSpeeds = withSpeeds;
        return this;
    }

    @Override
    protected Bicycle realization() {
        Bicycle bicycle = new Bicycle();
        bicycle.setFrameSize(frameSize);
        bicycle.setWithSpeeds(withSpeeds);
        return bicycle;
    }
}
