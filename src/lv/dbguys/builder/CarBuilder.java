package lv.dbguys.builder;

import lv.dbguys.Car;

/**
 * Created by boris on 01/07/16.
 */
public final class CarBuilder extends VehicleBuilder<CarBuilder, Car> {
    private double engineVolume;
    private boolean automaticTransmission;

    public static CarBuilder aCar() {
        return new CarBuilder();
    }

    public CarBuilder withEngineVolume(double engineVolume) {
        this.engineVolume = engineVolume;
        return this;
    }

    public CarBuilder withAutomaticTransmission(boolean automaticTransmission) {
        this.automaticTransmission = automaticTransmission;
        return this;
    }

    @Override
    protected Car realization() {
        Car car = new Car();
        car.setEngineVolume(engineVolume);
        car.setAutomaticTransmission(automaticTransmission);
        return car;
    }
}
